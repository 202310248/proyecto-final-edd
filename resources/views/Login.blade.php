<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HealthExpress</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
</head>
<body style="background: url('https://p4.wallpaperbetter.com/wallpaper/280/989/85/medicine-science-anatomy-wallpaper-preview.jpg') ; background-size: cover";>
    <div class="container">
        <div class="row justify-content-center pt-5 mt-5 m-1">
            <div class="col-md-6 col-sm-8 col-xl-4 col-lg-5 formulario">
            <form action="/Login_Validacion" method="POST">
                @csrf
                    <div class="form-group text-center pt-3">
                        <h1 class="fw-bold-md-4 pb-2" style="color: brown";>Iniciar sesion</h1>
                    </div>
                    <div class="form-group mx-sm-4 pt-3">
                        <input type="text" class="form-control"  placeholder="Usuario" name="Usuario">
                    </div>
                    <br><br>
                    <div class="form-group mx-sm-4 pb-3">
                        <input type="text" class="form-control" placeholder="Contraseña" name="Pasword">
                    </div>
                    <div class="form-group mx-sm-4 pb-2">
                        <input type="submit" class="btn btn-block ingresar" style="color: brown"; value= "INGRESAR" >
                    <br><br>
                    </div>
                    <div class="form-group mx-sm-4 text-right">
                        <span class=""><a href="#" class="olvide" style="color: black">¿Olvidaste tu contraseña?</a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
<h4>{{$sms}}</h4>
</body>
</html>